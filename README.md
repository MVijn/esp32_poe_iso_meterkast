#

project state:
- first design
- no code yet
- nothing is tested yet

project to learn using kicad, to make a pcb
to collect sensor data from:

- HeaderPins for  esp32_poe_iso[1]

inputs:
- dutch smartmeter aka slimme meter via P1 (inverted serial connection)
- DALLAS  DS18B20  temperature sensor (multiple sensors possible) 
- DHT  humity / temperature sensor
- 2x optocoupler PC814 [2]
- 1x Time of Flight distance sensor (vl53l0x or vl53l1x)
- 1x CCS811 air quality sensor (TOV en CO2)
- 1x MQ slot for MQ 2 3 4 etc
- 1x MIC for sound levels

outputs:
- led ws2811 / ws2811
- networks (http mqtt etc etc)

undefined pins:
- GPIO 14
- GPIO 15


More info / inspiration / sources

- arduino -> code
- MakerSpaceLeiden -> workplace
- Kicad -> PCB-design
- [1] https://www.olimex.com/Products/IoT/ESP32/ESP32-POE-ISO/open-source-hardware
- [2] https://github.com/Hans-Beerman/CompressorNode